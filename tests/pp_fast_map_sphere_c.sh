#!/bin/bash

rosservice call /sphere_c/path/control """
command: 'path'
points:
- values: [0.0, 0.0, 0.0, 0, 0, 0]
- values: [0.0, 0.0, 0.1, 0, 0, 0]
- values: [0.5, 0.0, 0.1, 0, 0, 0]
- values: [0.5, 21.0, 0.1, 0, 0, 0.0]
- values: [5.0, 21.0, 0.1, 0, 0, 0.0]
- values: [5.0, 0.0, 0.1, 0, 0, 0]
- values: [8.0, 0.0, 0.1, 0, 0, 0]
- values: [8.0, 21.0, 0.1, 0, 0, 0.0]
- values: [10.0, 21.0, 0.1, 0, 0, 0.0]
- values: [10.0, 0.0, 0.1, 0, 0, 0]
- values: [13.0, 0.0, 0.1, 0, 0, 0.0]
- values: [13.0, 21.0, 0.1, 0, 0, 0]
- values: [16.0, 21.0, 0.1, 0, 0, 0.0]
- values: [16.0, 0.0, 0.1, 0, 0, 0.0]
- values: [17.0, 0.0, 0.1, 0, 0, 0.0]
- values: [17.0, 21.0, 0.1, 0, 0, 0.0]
- values: [17.0, 21.0, 0.0, 0, 0, 0.0]

options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.60'
- key: 'altitude_offset'
  value: '5.00'
- key: 'timeout'
  value: '10000'
- key: 'map'
  value: 'False'
- key: 'geo'
  value: 'False'
"""
