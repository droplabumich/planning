#!/bin/bash

rosservice call sphere_a/path/control """
command: 'path'
points:
- values: [10.0, 0.0, 0.0, 0, 0, 0]
options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.90'
- key: 'timeout'
  value: '900'
- key: 'map'
  value: 'False'
- key: 'geo'
  value: 'False'
"""