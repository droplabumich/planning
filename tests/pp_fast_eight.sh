#!/bin/bash

rosservice call sphere_a/path/control """
command: 'path'
points:
- values: [0.0, 2.0, 0.5, 0, 0, 0]
- values: [2.0, 0.0, 0.5, 0, 0, 0]
- values: [2.0, 2.0, 0.5, 0, 0, 0]
- values: [0.0, 0.0, 0.5, 0, 0, 0]
- values: [0.0, 2.0, 0.5, 0, 0, 0]
options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.40'
- key: 'timeout'
  value: '900'
- key: 'map'
  value: 'False'
- key: 'geo'
  value: 'False'
"""