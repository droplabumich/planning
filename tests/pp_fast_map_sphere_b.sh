#!/bin/bash

rosservice call /sphere_b/path/control """
command: 'path'
points:
- values: [0.0, 0.0, 2.0, 0, 0, 0]
- values: [0.0, 0.0, 2.1, 0, 0, 0]
- values: [2.5, 0.0, 2.1, 0, 0, 0]
- values: [2.5, 21.0, 2.1, 0, 0, 0.0]
- values: [4.0, 21.0, 2.1, 0, 0, 0.0]
- values: [4.0, 0.0, 2.1, 0, 0, 0]
- values: [7.0, 0.0, 2.1, 0, 0, 0]
- values: [7.0, 21.0, 2.1, 0, 0, 0.0]
- values: [8.0, 21.0, 2.1, 0, 0, 0.0]
- values: [8.0, 0.0, 2.1, 0, 0, 0]
- values: [11.0, 0.0, 2.1, 0, 0, 0.0]
- values: [11.0, 21.0, 2.1, 0, 0, 0]
- values: [12.0, 21.0, 2.1, 0, 0, 0.0]
- values: [12.0, 0.0, 2.1, 0, 0, 0.0]
- values: [15.0, 0.0, 2.1, 0, 0, 0.0]
- values: [15.0, 21.0, 2.1, 0, 0, 0.0]
- values: [15.0, 21.0, 2.0, 0, 0, 0.0]

options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.60'
- key: 'altitude_offset'
  value: '5.00'
- key: 'timeout'
  value: '10000'
- key: 'map'
  value: 'False'
"""
