#!/bin/bash

rosservice call /path/control """
command: 'path'
points:
- values: [2.0, -8.0, 0.0, 0, 0, 0]
- values: [24.0, -8.0, 2.0, 0, 0, 0]
- values: [24.0, -16.0, 1.5, 0, 0, 0]
- values: [2.0, -16.0, 1.0, 0, 0, 0]
- values: [2.0, -24.0, 1.0, 0, 0, 0]
- values: [24.0, -24.0, 1.0, 0, 0, 0]
- values: [24.0, -32.0, 1.0, 0, 0, 0]
- values: [2.0, -32.0, 1.0, 0, 0, 0]
- values: [2.0, -40.0, 1.0, 0, 0, 0]
- values: [24.0, -40.0, 1.0, 0, 0, 0]
- values: [24.0, -48.0, 1.0, 0, 0, 0]
- values: [2.0, -48.0, 1.0, 0, 0, 0]
- values: [2.0, -56.0, 1.0, 0, 0, 0]
- values: [24.0, -56.0, 1.0, 0, 0, 0]
options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.75'
- key: 'timeout'
  value: '900'
- key: 'map'
  value: 'False'
- key: 'geo'
  value: 'False'
"""
