#!/bin/bash

rosservice call sphere_a/path/control """
command: 'path'
points:
- values: [21.306987023656,  -158.017827820439, 0.0, 0, 0, 0]
- values: [21.3070678177734, -158.017522323033, 0, 0, 0, 0]
- values: [21.3067556584378, -158.017727301938, 0, 0, 0, 0]
- values: [21.3069019307665, -158.01745025267, 4, 0, 0, 0]
- values: [21.3067876676425, -158.017741208843, 4, 0, 0, 0]
- values: [21.3069019307665, -158.01745025267, 4, 0, 0, 0]
- values: [21.3067709330799, -158.017733938386, 4, 0, 0, 0]

options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.40'
- key: 'timeout'
  value: '900'
- key: 'map'
  value: 'False'
- key: 'geo'
  value: 'True'
"""