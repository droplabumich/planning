#!/bin/bash

rosservice call /sphere_a/path/control """
command: 'path'
points:
- values: [1.0, -1.8, 0.0, 0, 0, 3.14]
- values: [1.0, -1.8, 0.0, 0, 0, 0]
- values: [1.0, -9.8, 0.0, 0, 0, 3.14]
- values: [4.4, -9.8, 0.0, 0, 0, 0]
- values: [4.4, -1.8, 0.0, 0, 0, 0]
options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.60'
- key: 'timeout'
  value: '300'
- key: 'map'
  value: 'False'
- key: 'geo'
  value: 'False'
"""
