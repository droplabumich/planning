#!/bin/bash

rosservice call /sphere_a/path/control """
command: 'path'
points:
- values: [0.0, 0.0, 1.0, 0, 0, 0]
- values: [0.0, 0.0, 1.1, 0, 0, 0]
- values: [2.0, 0.0, 0.1, 0, 0, 0]
- values: [2.0, 20.0, 1.1, 0, 0, 0.0]
- values: [4.0, 20.0, 1.1, 0, 0, 0.0]
- values: [4.0, 0.0, 0.1, 0, 0, 0]
- values: [6.0, 0.0, 0.1, 0, 0, 0]
- values: [6.0, 20.0, 1.1, 0, 0, 0.0]
- values: [8.0, 20.0, 1.1, 0, 0, 0.0]
- values: [8.0, 0.0, 0.1, 0, 0, 0]
- values: [10.0, 0.0, 0.1, 0, 0, 0.0]
- values: [10.0, 20.0, 1.1, 0, 0, 0]
- values: [12.0, 20.0, 1.1, 0, 0, 0.0]
- values: [12.0, 0.0, 0.1, 0, 0, 0.0]
- values: [14.0, 0.0, 0.1, 0, 0, 0.0]
- values: [14.0, 20.0, 1.1, 0, 0, 0.0]
- values: [14.0, 20.0, 1.0, 0, 0, 0.0]

options:
- key: 'mode'
  value: 'moving_waypoint'
- key: 'interpolation_method'
  value: 'linear'
- key: 'target_speed'
  value: '0.60'
- key: 'altitude_offset'
  value: '5.00'
- key: 'timeout'
  value: '10000'
- key: 'map'
  value: 'False'
- key: 'geo'
  value: 'False'
"""
