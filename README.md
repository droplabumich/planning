# PLANNING #

This repo consists of the ROS package for trajectory planning for the sphere autonomous vehicle.  It has a set of modules, nodes, configuration and launch files to run basic planners for the vehicle. Currently it consists of simple waypoint generation modules.
## Requirements ##

* A valid ROS installation 
* Python 2.7+
* Numpy 1.8+
* Scipy 0.13+

## Run planning module ##

This is to be run after the simulator was started.

In a new terminal, move to the /planning directory. Then execute the following commands:

### Single vehicle ###

* Load the waypoints: 

```
#!python

./tests/pp_fast_map_sphere_a.sh
```


* Start the path:

```
#!python

./scripts/path_cli_sphere_a.sh start
```

### Multiple vehicles ###

* Load the waypoints: 

```
#!python

./tests/pp_fast_map_sphere_a.sh
```

```
#!python

./tests/pp_fast_map_sphere_b.sh
```

```
#!python

./tests/pp_fast_map_sphere_c.sh
```


* Start the path:

```
#!python

./scripts/path_cli_sphere_a.sh start
```

```
#!python

./scripts/path_cli_sphere_b.sh start
```

```
#!python

./scripts/path_cli_sphere_c.sh start
```

## Expected behaviour in simulation ##

What to look for when starting the waypoint generator (to be checked after simulator is started):

1. When loading the waypoints check in RVIZ if small red, yellow and whie spheres appear on the visualisation.

2. When starting the path check in RVIZ if the vehicle moves. 

If all these checks, the waypoint generator and simulator work. 